
# docker-build-asterisk
This repository provides dockerized Asterisk PBX version 16 based on Debian Jessie

## Usage

Fork the gitlab repository

    git clone https://gitlab.com/ck.goh1439/asterisk-sip-docker.git
    cd docker-build-asterisk
    docker build -t build-asterisk:latest .

## Changes for this Asterisk 16

Changed version number in Dockerfile.

Removed libjansson-dev package.

Added '--with-jansson-bundled' in build-asterisk.sh
